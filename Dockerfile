FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code
WORKDIR /app/code
RUN curl -L https://github.com/seejohnrun/haste-server/tarball/f3838ab4a8b77b8ab3d26f1214b3a858682c5102 | tar -xz --strip-components 1 -f -

RUN npm install --production

ADD config.js /app/code/

COPY start.sh /app/code/start.sh

CMD /app/code/start.sh
